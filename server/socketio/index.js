const socketio = require("socket.io");
const serverVars = require("./serverVars");
const messages = require("./messageTypes");
const creatorDone = require("./creatorDone");
const tellToCreator = require("./tellToCreator");

module.exports = (server) => {
    
    const io = socketio.listen(server);

    const movement = require("./movement")(io);
    const playerActions = require("./playerAction")(io);
    const someoneScaped = require("./someoneScaped")(io);

    const left = require("./left")(io);
    const join = require("./join");

    io.on("connection", (socket) => {
        join(socket);
        
        socket.on("disconnect", () => {
            left(socket);
        });

        socket.on(messages.LABY_CREATED, (laby) => {
            creatorDone(socket, laby);
        });

        socket.on(messages.PLAYER_MOVEMENT, (data) => {
            movement(data);
        });

        socket.on(messages.PLAYER_ACTION, (data) => {
            playerActions(data);
        });

        socket.on(messages.SOMEONE_ESCAPED, (player) => {
            someoneScaped(socket, player);
        });

        socket.on(messages.ALL_CLEAR_FOR_CREATOR, (player) => {
            tellToCreator(socket)
        });
        socket.on(messages.ALL_CLEAR_FOR_OTHERS, (player) => {
            
        });
    });

    

};

