const messages = require("./messageTypes");
const Player = require("./player");
const createOrWait = require("./createOrWait");

let serverVars = require("./serverVars");
module.exports = (socket) => {
        let waitingInterval;
        let player = new Player(socket.id, "Player " + socket.id, 3);
        socket.emit(messages.PLAYER_ID, socket.id);
        serverVars.addPlayer(player);
        createOrWait(socket, player);
}