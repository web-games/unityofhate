const messages = require("./messageTypes");
let serverVars = require("./serverVars");
module.exports = (socket) => {
    let info = {xSize: serverVars.xSize, ySize: serverVars.ySize, wallLength: serverVars.wallLength};
    console.log(info);
    socket.emit(messages.BUILD_THE_LABY, info);
}