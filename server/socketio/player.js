"use strict";

class Player {
    
    constructor (id, name, range) {
        this.id = id;
        this.name = name;
        this.index = this.makeIndex(range);
        this.kills = 0;
        this.deaths = 0;
        this.score = 0;
        this.position = {x :0, y: 0, z : 0};
    }  
    makeIndex (range) {
        return Math.floor(Math.random () * range);
    }
}

module.exports = Player;