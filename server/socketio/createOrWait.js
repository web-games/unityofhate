let serverVars = require("./serverVars");
const messages = require("./messageTypes");
const tellToCreator = require("./tellToCreator");

module.exports = (socket, player) => {
    let waitingInterval;
    console.log(serverVars.connections);
    if (serverVars.connections < 2) {
        tellToCreator(socket);        
    }

    else {
        console.log("Wait until laby is ready");
        waitingInterval = setInterval(() => {
            // console.log("is it ok: ", serverVars.laby.exit);
            if (serverVars.laby.exit != null) {
                serverVars.randomizeSpawnPosition(player);
                socket.broadcast.emit(messages.NEW_PLAYER_HERE, player);
                
                socket.emit(messages.THE_PLAYERS, {players: serverVars.players});
                socket.emit(messages.LABY_TO_OTHERS, serverVars.laby);

                clearInterval(waitingInterval);
            }
        }, 500);
    }

}