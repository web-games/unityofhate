const messages = require("./messageTypes");
let serverVars = require("./serverVars");
// const logger = require("tracer").console();

module.exports = (io) => {
    return (socket) => {
        let player = serverVars.find(socket.id);
        serverVars.removePlayer(socket.id);
        io.sockets.emit(messages.PLAYER_LEFT, player);
        console.log(serverVars.players);
    }
    
}

