const messages = require("./messageTypes");
module.exports = (io) => {
    return (data) => {
        io.sockets.emit(messages.PLAYER_ACTION, data);    
    }
}