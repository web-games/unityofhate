const messages = require("./messageTypes");
const tellToCreator = require("./tellToCreator");

module.exports = (io) => {

    return (socket, player) => {
        socket.emit(messages.CREATOR_CLEAN_THE_MESS, {ids: [], tags: ["MazeWall", "Exit", "Player"]});
        socket.broadcast.emit(messages.OTHERS_CLEAN_THE_MESS, {ids: [], tags: ["MazeWall", "Exit", "Player"]});
    }
}