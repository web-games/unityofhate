let serverVars = require("./serverVars");
const messages = require("./messageTypes");

module.exports = (socket, laby) => {
    serverVars.laby = laby;
    serverVars.sortExit()
    .then((exit) => {
        serverVars.randomizeAllSpawnPosition();
        socket.emit(messages.EXIT_TO_CREATOR, serverVars.laby);
        socket.emit(messages.THE_PLAYERS, {players: serverVars.players});
        
        socket.broadcast.emit(messages.THE_PLAYERS, {players: serverVars.players});
        socket.broadcast.emit(messages.LABY_TO_OTHERS, serverVars.laby);
    })
    .catch(err => console.log(err));
}