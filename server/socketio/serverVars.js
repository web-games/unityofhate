"use strict";
const Player = require("./player");
const _ = require("lodash");

class ServerVars {
    constructor (xSize, ySize, wallLength) {
        this.xSize = xSize;
        this.ySize = ySize;
        this.wallLength = wallLength;
        this.players = [];
        this.roundEnded = false;
        this.connections = 0;
        this.laby = {cells: [], walls: [], exit: null};
    }

    addPlayer (player) {
        this.players.push(player);
        this.connections++;
    }
    removePlayer (id) {
        this.players = this.players.filter((p) => p.id !== id);
        this.connections--;
    }
    find (id)  {
        return this.players.find(p => (p.id === id) );
    }
    updatePosition (player) {
        this.players = this.players.map(p => {
            if (p.id == player.id) {
                p.position = player.position;
            }
            return p;
        });
    }

    randomizeCell (cells) {

        let choosen = Math.floor(Math.random() * cells.length);
        let cell = cells[choosen];
        while(cell.south == null || cell.south.position.x == 0) {
            choosen = Math.floor(Math.random() * cells.length);
            cell = cells[choosen];
        }
        return cell;
    }


    sortExit () {
        return new Promise( (resolve, reject) => {
            let cells = this.laby.cells.slice(this.laby.cells.length / 2, this.laby.cells.length);
            
            let cell = this.randomizeCell(cells);
            
            this.laby.exit = {
                x: cell.south.position.x, 
                y: 1.25, 
                z: cell.south.position.z + this.wallLength / 2
            }

            if (this.laby.exit.x == null) return reject(this.laby.exit)
            return resolve (this.laby.exit);
        });
    }

    randomizeSpawnPosition (player) {
        let cells = this.laby.cells.slice(0, (this.laby.cells.length /2));
        
        let cell = this.randomizeCell(cells);

        let  newPosition = {
            x: cell.south.position.x,
            y: 0.5,
            z: cell.south.position.z + this.wallLength / 2
        };
        player.position = newPosition;
        this.updatePosition(player);
    }

    randomizeAllSpawnPosition () {
        this.players.forEach((player) => {
            this.randomizeSpawnPosition(player);
        });
        
    }
    
}

let serverVars = new ServerVars(6, 6, 5);
// let serverVars = new ServerVars(32, 32, 5);
// let serverVars = new ServerVars(16, 16, 5);

module.exports = serverVars;