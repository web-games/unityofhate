﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wall {
	public Vector3 position;
	public Vector3 eulerAngle;

	public Wall() {
	}

	public Wall(Vector3 position, Vector3 eulerAngle) {
		this.position = position;
		this.eulerAngle = eulerAngle;
	}
		
 }
