﻿using System;
using UnityEngine;

[System.Serializable]
public class CellToExport {
	public bool visited;
	public Wall north;
	public Wall east;
	public Wall west;
	public Wall south;
}


