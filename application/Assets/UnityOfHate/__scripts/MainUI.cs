﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUI : MonoBehaviour {

	private GameData gameData;
	private Networking netWorking;
	public GameObject roundEndedPanel;
	public GameObject playersPanel;

	public Text winnerText;
	public RawImage winnerPic;
	public Text restartTimerText;
	public int restartTimer;

	Coroutine restartingRoutine;
	bool restarting = false;

	void Awake () {
		gameData = GameObject.FindObjectOfType<GameData> ();
	}
	void Start () {
		//restartingRoutine = StartCoroutine (RestartTimer ());
	}
		

	public void ShowRoundEnded (PlayerSetup winner) {
		restartTimer = 10;
		roundEndedPanel.SetActive (true);
		winnerText.text = winner.name;
		winnerPic.texture = gameData.playerPics [winner.index];
		restartTimerText.text = "" + restartTimer;
		restarting = true;
		restartingRoutine = StartCoroutine (RestartTimer ());
	}


	IEnumerator RestartTimer() {
		while (restartTimer > 0) {
			restartTimerText.text = "" + restartTimer;
			restartTimer--;

			yield return new WaitForSecondsRealtime (1f);
		}
		roundEndedPanel.SetActive (false);
		StopCoroutine (restartingRoutine);
	}


}
