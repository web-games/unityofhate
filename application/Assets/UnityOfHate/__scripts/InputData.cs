﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InputData {

	public string id;
	public string type;
	public bool value;
	public float delay;

	public InputData () {
		
	}

	public InputData (string type, bool value, float delay) {
		this.type = type;
		this.value = value;
		this.delay = delay;
	}
}
