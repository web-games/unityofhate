﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Laby {
	
	public List<CellToExport> cells;
	public List<Wall> walls;
	public Vector3 exit;

	public Laby (List<CellToExport> cells, List<Wall> walls) {
		this.cells = cells;
		this.walls = walls;
	}
}
