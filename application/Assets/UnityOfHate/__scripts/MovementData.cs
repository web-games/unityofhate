﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MovementData {
	public string id;
	public Vector3 position;

	public MovementData(string id, Vector3 position){
		this.id = id;
		this.position = position;
	}
}
