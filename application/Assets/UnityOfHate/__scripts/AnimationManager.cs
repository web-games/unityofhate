﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Runtime.InteropServices;

public class AnimationManager : MonoBehaviour {
	public Animator anim;
	bool isWalking;

	public NavMeshAgent agent;
	public PlayerActions actions;

	private Coroutine loopUpdateRoutine;

	void Start () {
		anim = this.transform.GetComponent<Animator> ();
		isWalking = anim.GetBool ("isWalking");
		loopUpdateRoutine = StartCoroutine (LoopUpdate ());
	}
		
	IEnumerator LoopUpdate () {
		while (true) {

			if (agent.velocity.magnitude > 0.3f) {
				anim.SetBool ("isWalking", true);
			}
			else {
				anim.SetBool ("isWalking", false);
			}
			yield return new WaitForEndOfFrame ();
		}
	}
}
