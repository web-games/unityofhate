﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataToDestroy {

	public string[] tags;
	public string[] ids;
}
