﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cell {

	public bool visited;
	public Wall north;
	public Wall east;
	public Wall west;
	public Wall south;
}
