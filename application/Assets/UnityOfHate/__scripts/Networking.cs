﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnitySocketIO;
using UnitySocketIO.Events;
using UnityEngine.AI;

public class Networking : MonoBehaviour {
	public SocketIOController io;
	private NetworkActions networkActions;
	void Start() {
		
		networkActions = this.GetComponent<NetworkActions> ();
		io.Connect ();

		io.On ("connect", (SocketIOEvent e) => {
			Debug.Log ("SocketIO connected");
		});

		io.On (MessageTypes.PLAYER_ID, (SocketIOEvent e) => {
			networkActions.GiveMeID(e.data);
		});

		io.On (MessageTypes.BUILD_THE_LABY, (SocketIOEvent e) => {
			networkActions.CreateAndExportLaby(e.data);
		});

		io.On (MessageTypes.EXIT_TO_CREATOR, (SocketIOEvent e) => {
			networkActions.GiveMeTheExit(e.data);
		});

		io.On (MessageTypes.LABY_TO_OTHERS, (SocketIOEvent e) => {
			networkActions.GiveMeTheLab (e.data);
		});

		io.On (MessageTypes.THE_PLAYERS, (SocketIOEvent e) => {
			networkActions.GiveMeThePlayers(e.data);
		});

		io.On (MessageTypes.NEW_PLAYER_HERE, (SocketIOEvent e)  => {
			networkActions.GiveMeTheNewGuy(e.data);
		});

		io.On (MessageTypes.PLAYER_LEFT, (SocketIOEvent e) => {
			networkActions.GiveMeTheGuyToKick (e.data);
		});

		io.On (MessageTypes.PLAYER_MOVEMENT, (SocketIOEvent e) => {
			networkActions.GiveMeTheGuyMovement (e.data);
		});

		io.On (MessageTypes.PLAYER_ACTION, (SocketIOEvent e) => {
			networkActions.GiveMeTheGuyAction (e.data);
		});

		io.On (MessageTypes.ROUND_HAS_ENDED, (SocketIOEvent e) => {
			networkActions.GiveMeTheRoundEndedData (e.data);
		});

		io.On (MessageTypes.CREATOR_CLEAN_THE_MESS, (SocketIOEvent e) => {
			networkActions.GiveMeTheCreatorDataToDestroy (e.data);
		});

		io.On (MessageTypes.OTHERS_CLEAN_THE_MESS, (SocketIOEvent e) => {
			networkActions.GiveMeTheOthersDataToDestroy (e.data);
		});


	}
	
}
