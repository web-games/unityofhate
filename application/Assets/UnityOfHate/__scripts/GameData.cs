﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public  class GameData : MonoBehaviour {
	public  Transform[] playerPrefabs;
	public 	Texture[] playerPics;
	public string mazeWallTag;
	public string exitTag;
	public string id;
	public PlayerSetup player;
	public PlayerSetup winner;
}
