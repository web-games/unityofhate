﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CheckingCollisions : MonoBehaviour {
	public NetworkActions netActions;
	public NavMeshAgent agent;
	public GameData gameData;
	public bool exitFounded = false;

	void Awake() {
		gameData = GameObject.FindObjectOfType<GameData> ();
		netActions = GameObject.FindObjectOfType<NetworkActions> ();
	}
	void Start () {
		
	}
		
	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == gameData.exitTag && !exitFounded) {
			exitFounded = true;
			console.log (other.gameObject.name);
			console.log ("END ROUND");
			netActions.IFoundTheExit ();
		}
	}
}
