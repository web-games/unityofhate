﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ApplyMovement : MonoBehaviour {
	NavMeshAgent agent;
	Rigidbody body;
	void Awake() {
		agent = this.GetComponent<NavMeshAgent> ();
		body = this.GetComponent<Rigidbody> ();
	}

	void Start() {
		StartCoroutine (NormalizeVelocity ());
	}

	public void Translate (Vector3 position) {
		agent.isStopped = false;
		bool value = agent.SetDestination (position);
	}

	IEnumerator NormalizeVelocity () {
		while (true) {
			body.angularVelocity = Vector3.zero;
			body.velocity = Vector3.zero;
			yield return new WaitForEndOfFrame ();
		}
	}

}
