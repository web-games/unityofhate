﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PlayerActions {

	public string name;
	public bool value;

	public PlayerActions (string name, bool value){
		this.name = name;
		this.value = value;
	}
}
