﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySocketIO;
using UnitySocketIO.Events;
using UnityEngine.AI;

public class NetworkActions : MonoBehaviour {

	public SocketIOController io;
	ServerVars server;
	NetworkInstatiate netInstantiate;
	NetworkMaze mazeCreator;
	GameData gameData;


	void Awake () {
		netInstantiate = this.GetComponent<NetworkInstatiate> ();
		mazeCreator = this.GetComponent<NetworkMaze> ();
		gameData = GameObject.FindObjectOfType<GameData> ();
	}

	//DOWNLOAD 
	public void GiveMeID (string data) {
		gameData.id = data.Replace ("\"", "");
	}
		
	public void CreateAndExportLaby (string json) {
		server = JsonUtility.FromJson<ServerVars> (json);
		mazeCreator.Init (server.xSize, server.ySize, server.wallLength);
		StartCoroutine (ExportLabyCallback ());
	}

	private IEnumerator ExportLabyCallback () {
		while (!mazeCreator.canIExportWalls) {
			yield return new WaitForEndOfFrame ();
		}

		Laby laby = new Laby (mazeCreator.cellsToExport, mazeCreator.wallsToExport);
		string data = JsonUtility.ToJson (laby);
		console.log ("new laby", data);
		io.Emit (MessageTypes.LABY_CREATED, data);

	}

	public void GiveMeTheLab (string data) {
		Laby laby = JsonUtility.FromJson<Laby>(data);
		netInstantiate.CreateMazeWalls(laby.walls);
		netInstantiate.InstantiateSceneObject(0, laby.exit);
	}

	public void GiveMeTheExit (string data) {
		Laby laby = JsonUtility.FromJson<Laby>(data);
		netInstantiate.InstantiateSceneObject(0, laby.exit);
	}

	public void GiveMeThePlayers (string data) {
		console.log ("Give me the players: ", data);
		PlayersContainer container = JsonUtility.FromJson<PlayersContainer> (data);
		netInstantiate.CreatePlayer (container.players);
	}

	public void GiveMeTheNewGuy (string data) {
		PlayerSetup player = JsonUtility.FromJson<PlayerSetup> (data);
		netInstantiate.CreatePlayer (player);
	}

	public void GiveMeTheGuyToKick (string data) {
		PlayerSetup player = JsonUtility.FromJson<PlayerSetup> (data);
		netInstantiate.RemovePlayer (player.id);
	}

	public void GiveMeTheGuyMovement (string data) {
		MovementData moveData = JsonUtility.FromJson<MovementData> (data);
		GameObject player = GameObject.Find (moveData.id);
		if (player) player.GetComponent<ApplyMovement>().Translate (moveData.position);
	}

	public void GiveMeTheGuyAction (string data) {
		InputData inputData = JsonUtility.FromJson<InputData> (data);
		GameObject player = GameObject.Find (inputData.id);
		if (player){
			PlayerClass playerClass = player.GetComponent<PlayerClass>();
			StartCoroutine (ActionCallback(player, inputData));
		}
	}

	private IEnumerator ActionCallback (GameObject player, InputData data){
		Animator anim = player.GetComponent<AnimationManager>().anim;
		NavMeshAgent agent = player.GetComponent<NavMeshAgent> ();
		agent.isStopped = true;
		anim.SetBool (data.type,  data.value); 
		yield return new WaitForEndOfFrame();
		anim.SetBool (data.type, false);
	}


	public void GiveMeTheRoundEndedData (string data) {
		PlayerSetup player = JsonUtility.FromJson<PlayerSetup> (data);
		gameData.winner = player;
	}

	public void GiveMeTheCreatorDataToDestroy (string data) {
		DataToDestroy dataToDestroy = JsonUtility.FromJson<DataToDestroy> (data);
		netInstantiate.DestroyObjectsByTag (dataToDestroy.tags);
		StartCoroutine (AllClearCallback (MessageTypes.ALL_CLEAR_FOR_CREATOR));
	}

	public void GiveMeTheOthersDataToDestroy (string data) {
		DataToDestroy dataToDestroy = JsonUtility.FromJson<DataToDestroy> (data);
		netInstantiate.DestroyObjectsByTag (dataToDestroy.tags);
		StartCoroutine (AllClearCallback (MessageTypes.ALL_CLEAR_FOR_OTHERS));
	}

	private IEnumerator AllClearCallback (string message) {
		while(!netInstantiate.destructionSuccess) {
			yield return new WaitForEndOfFrame ();
		}

		string data = JsonUtility.ToJson (gameData.player);
		io.Emit (message, data);

		console.log ("all clear", message);

	}

	//UPLOAD
	public void SendMovement (Vector3 position) {
		MovementData moveData = new MovementData (gameData.id, position);
		string data = JsonUtility.ToJson (moveData);
		io.Emit (MessageTypes.PLAYER_MOVEMENT, data);
	}

	public void SendAction (InputData data) {
		data.id = gameData.id;
		string json = JsonUtility.ToJson (data);
		io.Emit (MessageTypes.PLAYER_ACTION, json);
	}

	public void IFoundTheExit () {
		string data = JsonUtility.ToJson (gameData.player);
		io.Emit (MessageTypes.SOMEONE_ESCAPED, data);
	}
}
