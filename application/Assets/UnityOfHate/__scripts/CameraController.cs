﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class CameraController : MonoBehaviour {

	public Transform target;
	public Transform secondTarget;
	public float smoothTime = 0.3f;
	public float rangeView;
	private Vector3 velocity = Vector3.zero;
	public PlayerClass playerClass;
	void Start () {
		rangeView = transform.position.y;
	}

	public void Init(Transform player) {
		target = player;
		playerClass = target.GetComponent<PlayerClass> ();
		this.transform.rotation = Quaternion.Euler (playerClass.camRotation);
		StartCoroutine (LockRotation());
	}


	IEnumerator LockRotation(){

		while (true) {
			if (target != null) {
				Vector3 goalPos = target.position;
				//porque o player esta sendo destruido em algum momento
				goalPos.y = rangeView + playerClass.rangeViewY;
				goalPos.z = goalPos.z - playerClass.rangeViewZ;
				transform.position = Vector3.SmoothDamp (transform.position, goalPos, ref velocity, smoothTime);	
			}
			else  transform.position = Vector3.SmoothDamp (transform.position, secondTarget.position, ref velocity, 2f);
			yield return new WaitForEndOfFrame ();
		}


	

	}
		
}
