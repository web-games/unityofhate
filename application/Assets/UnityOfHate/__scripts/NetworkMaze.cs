﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkMaze : MonoBehaviour {


	public GameObject wall;
	public float wallLength;
	public int xSize;
	public int ySize;
	public Cell[] cells;
	public List<CellToExport> cellsToExport;
	public List<Wall> wallsToExport;
	public bool canIExportWalls;

	public int currentCell = 0;

	int totalCells;
	int visitedCells;
	bool staredBuilding;
	int currentNeighbour;
	Vector3 initialPos;
	public GameObject wallHolder;
	List<int> lastCells;
	int backingUp = 0;
	int wallToBreak = 0;

	Coroutine exportWallsRoutine;


	[System.Serializable]
	public class Cell {
		public bool visited;
		public GameObject north; //1
		public GameObject east; //2
		public GameObject west; //3
		public GameObject south;//4
	}
		

	public void Init(int xSize, int ySize, float wallLength) {
		this.xSize = xSize;
		this.ySize = ySize;
		this.wallLength = wallLength;
		visitedCells = 0;
		totalCells = 0;
		staredBuilding = false;
		currentNeighbour = 0;
		canIExportWalls = false;

		CreateWalls ();
		CreateCells ();
		CreateMaze ();
		wallsToExport = new List<Wall> ();
		exportWallsRoutine = StartCoroutine (ExportWalls ());
	}

	private IEnumerator ExportWalls () {
		yield return new WaitForSeconds (2f);
//		console.log ("starting to export with: ", wallHolder.transform.childCount); 
		for (int i = 0; i < wallHolder.transform.childCount; i++ ) {
			Transform child = wallHolder.transform.GetChild (i);
			Wall wall = new Wall (child.position, child.rotation.eulerAngles);
			wallsToExport.Add (wall);
		}

		foreach (Cell c in cells) {

			CellToExport cellToExport = new CellToExport();

			if (c.east != null) {
				Wall east = new Wall (c.east.transform.position, c.east.transform.rotation.eulerAngles);
				cellToExport.east = east;
			}
			if (c.west != null) {
				Wall west = new Wall (c.west.transform.position, c.west.transform.rotation.eulerAngles);
				cellToExport.west = west;
			}
			if (c.south != null) {
				Wall south = new Wall (c.south.transform.position, c.south.transform.rotation.eulerAngles);
				cellToExport.south = south;
			}
			if (c.north != null) {
				Wall north = new Wall (c.north.transform.position, c.north.transform.rotation.eulerAngles);
				cellToExport.north = north;
			}

			cellsToExport.Add (cellToExport);
		}

		canIExportWalls = true;
		StopCoroutine (exportWallsRoutine);
//		console.log ("DONE!", wallHolder.transform.childCount); 
	}



	void CreateWalls () {
		wallHolder.name = "Maze";

		string debug = "";

		initialPos = new Vector3 (( (float)-xSize / 2) + wallLength / 2, 0, ( (float)-ySize / 2) + wallLength / 2);

		Vector3 myPos = initialPos;

		GameObject tempWall;

		for (int i = 0; i < ySize; i++) {
			for (int j = 0; j <= xSize; j++) {
				myPos = new Vector3 (initialPos.x + (j * wallLength) - wallLength / 2, 0, initialPos.z + (i * wallLength) - wallLength / 2);
				debug += JsonUtility.ToJson (myPos);
				tempWall = Instantiate (wall, myPos, Quaternion.identity) as GameObject;
				tempWall.transform.parent = wallHolder.transform;
			}
		}
		//for y axis
		for (int i = 0; i <= ySize; i++) {
			for (int j = 0; j < xSize; j++) {
				myPos = new Vector3 (initialPos.x + (j * wallLength), 0, initialPos.z + (i * wallLength) - wallLength);
				debug += JsonUtility.ToJson (myPos);
				tempWall = Instantiate (wall, myPos, Quaternion.Euler(0, 90, 0) ) as GameObject;
				tempWall.transform.parent = wallHolder.transform;
			}
		}


	}

	void CreateCells () {

		string debug = "";

		lastCells = new List<int> ();
		lastCells.Clear ();
		totalCells = xSize * ySize;

//		console.log ("totalCells: ", xSize * ySize);

		int children = wallHolder.transform.childCount;
//		print ("children" + children);
		GameObject[] allWalls = new GameObject[children];
		cells = new Cell[xSize * ySize];

//		console.log ("cells length: ", cells.Length);
		int eastWeastProcess = 0;
		int childProcess = 0;
		int termCount = 0;

		//Get All the children 
		for (int i = 0; i < children; i ++) {
			allWalls [i] = wallHolder.transform.GetChild (i).gameObject;
			debug += "\n" + JsonUtility.ToJson ( allWalls [i].transform.position);

		}

		for (int cellprocess = 0; cellprocess < cells.Length; cellprocess++) {

			cells [cellprocess] = new Cell ();
			cells [cellprocess].east = allWalls [eastWeastProcess];
			cells [cellprocess].south = allWalls [childProcess + (xSize+1) * ySize];


			if (termCount == xSize) {
				eastWeastProcess += 2;
				termCount = 0;
			} else {
				eastWeastProcess++;
			}
			termCount++;
			childProcess++;

			cells [cellprocess].west = allWalls [eastWeastProcess];
			cells [cellprocess].north = allWalls [ (childProcess + (xSize+1) * ySize) + xSize -1];

		}
			
			
	}

	void CreateMaze () {
		while (visitedCells < totalCells) {
			if (staredBuilding) {
				GiveMeNeighbour ();
				if (cells [currentNeighbour].visited == false && cells [currentCell].visited == true) {
					BreakWall ();
					cells [currentNeighbour].visited = true;
					visitedCells++;
					lastCells.Add (currentCell);
					currentCell = currentNeighbour;
					if (lastCells.Count > 0) {
						backingUp = lastCells.Count - 1;
					}
				}
			} 
			else {
				currentCell = Random.Range (0, totalCells);
				cells [currentCell].visited = true;
				visitedCells++;
				staredBuilding = true;
			}

		}


		GiveMeNeighbour ();
//		Debug.Log ("finished!!!!");

	}

	void BreakWall() {
		switch (wallToBreak) {
		case 1:
			Destroy (cells [currentCell].north);
			break;
		case 2:
			Destroy (cells [currentCell].east);
			break;
		case 3:
			Destroy (cells [currentCell].west);
			break;
		case 4:
			Destroy (cells [currentCell].south);
			break;

		}
	}

	void GiveMeNeighbour () {

		int length = 0;
		int[] neighgours = new int[4];
		int[] connectingWall = new int[4];
		int check = 0;
		check = ((currentCell + 1) / xSize);
		check -= 1;
		check *= xSize;
		check += xSize;
		//west 
		if (currentCell + 1 < totalCells &&  (currentCell +1) != check) {
			if (cells[currentCell + 1].visited == false) {
				neighgours [length] = currentCell + 1;
				connectingWall [length] = 3;
				length++;
			}
		}

		//east
		if (currentCell - 1 >= 0 && currentCell != check) {
			if (cells[currentCell - 1].visited == false) {
				neighgours [length] = currentCell - 1;
				connectingWall [length] = 2;
				length++;
			}
		}

		//north
		if (currentCell + xSize < totalCells) {
			if (cells[currentCell + xSize].visited == false) {
				neighgours [length] = currentCell +xSize;
				connectingWall [length] = 1;
				length++;
			}
		}

		//south
		if (currentCell - xSize >= 0) {
			if (cells[currentCell - xSize].visited == false) {
				neighgours [length] = currentCell - xSize;
				connectingWall [length] = 4;
				length++;
			}
		}

		if (length != 0) {
			int theChoosenOne = Random.Range (0, length);
			currentNeighbour = neighgours [theChoosenOne];
			wallToBreak = connectingWall [theChoosenOne];

		} 
		else {
			if (backingUp > 0) {
				currentCell = lastCells [backingUp];
				backingUp--;
			}
		}

		//		for (int i = 0; i < length; i++) {
		//			Debug.Log (neighgours[i]);	
		//		}
		//
	} 
}
