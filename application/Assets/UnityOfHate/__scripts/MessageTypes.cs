﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MessageTypes  {
	public static string PLAYER_ID = "PLAYER_ID";
	public static string BUILD_THE_LABY = "BUILD_THE_LABY";
	public static string LABY_CREATED = "LABY_CREATED";
	public static string EXIT_TO_CREATOR = "EXIT_TO_CREATOR";
	public static string LABY_TO_OTHERS = "LABY_TO_OTHERS";
	public static string THE_PLAYERS = "THE_PLAYERS";
	public static string NEW_PLAYER_HERE = "NEW_PLAYER_HERE";
	public static string PLAYER_LEFT = "PLAYER_LEFT";
	public static string PLAYER_MOVEMENT = "PLAYER_MOVEMENT";
	public static string PLAYER_ACTION = "PLAYER_ACTION";
	public static string SOMEONE_ESCAPED = "SOMEONE_ESCAPED";
	public static string ROUND_HAS_ENDED = "ROUND_HAS_ENDED";

	public static string CREATOR_CLEAN_THE_MESS = "CREATOR_CLEAN_THE_MESS";
	public static string OTHERS_CLEAN_THE_MESS = "OTHERS_CLEAN_THE_MESS";

	public static string ALL_CLEAR_FOR_CREATOR = "ALL_CLEAR_FOR_CREATOR";
	public static string ALL_CLEAR_FOR_OTHERS = "ALL_CLEAR_FOR_OTHERS";
}
