﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClass : MonoBehaviour {
	public PlayerSetup setup;

	public int hp;
	public float power;
	public float speed;
	public float primAttackDelay;
	public float secAttackDelay;
	public bool walkWhileAttack;
	public bool attacking;
	public float rangeViewY;
	public float rangeViewZ;
	public Vector3 camRotation;

}
