﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerSetup {
	public string id;
	public string name;
	public int index;
	public int kills;
	public int deaths;
	public int score;
	public Vector3 position;
}
