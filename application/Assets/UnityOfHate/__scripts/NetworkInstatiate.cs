﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class NetworkInstatiate : MonoBehaviour {
	public GameData gameData;
	Transform[] prefabs;
	public Transform wallPrefab;
	public GameObject wallHolder;
	public GameObject cellHolder;
	public Camera cam;
	public ServerVars server;
	public Transform[] sceneObjects;
	public List<PlayerSetup> players;
	public bool destructionSuccess = false;
	void Awake () {
		prefabs = gameData.playerPrefabs;
	}

	public void AddPlayer(PlayerSetup p) {
		players.Add (p);
	}
	private int FindPlayer(string id) {
		int found = -1;
		for (int i = 0; i < players.Count; i ++) {
			if (players[i].id.Contains (id)) {
				found = i;
			}
		}
		return found;
	}
	public void RemovePlayer(string id) {
		int index = FindPlayer (id);
		if (index >  -1) {
			players.RemoveAt (index);
			DestroyObjectByID (id);
		}
	}

	public Transform CreatePlayer(PlayerSetup setup) {
		Transform prefab = prefabs [setup.index];
		Transform transform = (Transform) Instantiate (prefab, setup.position, prefab.rotation);
		transform.name = setup.id;
		PlayerClass playerClass = transform.GetComponent<PlayerClass> ();
		playerClass.setup = setup;

		if (gameData.id.Contains (setup.id)) {
			CameraController camController = cam.GetComponent<CameraController> ();
			transform.gameObject.AddComponent<CheckingCollisions> ();
			transform.gameObject.AddComponent<PlayerInput> ();
			camController.Init (transform);
			gameData.player = setup;
		}
		AddPlayer (setup);
		return transform;
	}


	public void CreatePlayer(List<PlayerSetup> setups) {
		foreach (PlayerSetup setup in setups) {
			CreatePlayer (setup);
		}
	}

	public void InstantiateSceneObject (int prefabIndex, Vector3 position) {
		Instantiate (sceneObjects[prefabIndex], position, Quaternion.identity);
	}

	public void DestroyObjectByID(string id) {
		GameObject g = GameObject.Find (id);
		Destroy (g);
	}

	void FindAndDestroyByTag(string tag) {
		GameObject[] gameObs = GameObject.FindGameObjectsWithTag (tag);
		foreach (GameObject g in gameObs) {
			//Debug.Log (" detroi gameObject: " + g);
			Destroy (g);	
		}
	}

	public void DestroyObjectsByTag (string[] tags) {
		destructionSuccess = false;
		foreach (string tag in tags) {
			FindAndDestroyByTag (tag);
		}
		destructionSuccess = true;
	}

	public void CreateMazeWall(Wall wall, GameObject parent) {
		Transform t = Instantiate (wallPrefab, wall.position, Quaternion.Euler(wall.eulerAngle)) as Transform;
//		t.parent = parent.transform;
	} 

	public void CreateMazeWall(Wall wall, GameObject parent, string name) {
		Transform t = Instantiate (wallPrefab, wall.position, Quaternion.Euler (wall.eulerAngle)) as Transform;
		t.name = name;
		//t.parent = parent.transform;
	}

	public void CreateMazeWall(GameObject wall, GameObject parent) {
		Transform t = Instantiate (wallPrefab, wall.transform.position, wall.transform.rotation) as Transform;
		t.name = name;
		//t.parent = parent.transform;
	}

	public void CreateMazeWalls (List<Wall> walls) {

		print ("walls count: " + walls.Count);

		foreach (Wall wall in walls) {
			CreateMazeWall (wall, wallHolder);
		}
	}

	public void CreateMazeCells (List<Cell> cells ) {

		print ("cells count: " + cells.Count); 
		foreach (Cell cell in cells) {
			CreateMazeWall (cell.north, cellHolder);
			CreateMazeWall (cell.east, cellHolder);
			CreateMazeWall (cell.west, cellHolder);
			CreateMazeWall (cell.south, cellHolder);

		}

	}

	public void CreateLabyCells (List<CellToExport> cells) {
		print ("cells laby count: " + cells.Count); 
		foreach (CellToExport cell in cells) {
			CreateMazeWall (cell.north, cellHolder);
			CreateMazeWall (cell.east, cellHolder);
			CreateMazeWall (cell.west, cellHolder);
			CreateMazeWall (cell.south, cellHolder);
		}
	}

}
