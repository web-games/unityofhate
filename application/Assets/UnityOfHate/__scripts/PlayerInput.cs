﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

	private NetworkActions networkActions;
	private float primAttackDelay;
	private float secAttackDelay;
	private InputData data;
	private PlayerActions actions;
	private PlayerClass playerClass;
	private bool performingAction = false;

	void Awake() {
		networkActions = GameObject.FindObjectOfType<NetworkActions>();
		playerClass = this.GetComponent<PlayerClass> ();
		primAttackDelay = playerClass.primAttackDelay;
		secAttackDelay = playerClass.secAttackDelay;
	}

	void Start() {
		StartCoroutine (MovementInput (.1f));
		StartCoroutine (SkillsInput ());
	}

	IEnumerator MovementInput (float frame) {
		while (true) {
			if (Input.GetMouseButton (0) && performingAction == false) {
				SendHitpoint ();
			}

			yield return new WaitForSeconds (frame);
		}
	}

	void SendHitpoint  () {
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 100)) {
			networkActions.SendMovement (hit.point);
		}
	}

	IEnumerator SkillsInput () {
		while (true) {

			if (Input.GetMouseButton (1)) {
				data = new InputData ("attack1", true, playerClass.primAttackDelay);
				networkActions.SendAction(data);
				performingAction = true;
				yield return new WaitForSeconds (primAttackDelay);
			}

			if (Input.GetKey (KeyCode.Q)) {
				data = new InputData ("attack2", true, playerClass.secAttackDelay);
				networkActions.SendAction(data);
				performingAction = true;
				yield return new WaitForSeconds (secAttackDelay);
			}

			if (Input.GetKey(KeyCode.W)) {
				data = new InputData ("attack3", true, playerClass.secAttackDelay);
				networkActions.SendAction(data);
				performingAction = true;
				yield return new WaitForSeconds (secAttackDelay);
			}

			performingAction = false;
			yield return new WaitForEndOfFrame ();

		}
	}

}
