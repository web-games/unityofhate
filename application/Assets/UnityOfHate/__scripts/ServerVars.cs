﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ServerVars {
	public int xSize; 
	public int ySize;
	public float wallLength;
}
