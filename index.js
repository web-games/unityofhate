const express = require("express");
const http = require("http");
const path = require("path");
const morgan = require("morgan");


const handler = express();

const port = process.env.PORT || 5000;
var server = http.createServer(handler);


handler.use(express.static( __dirname + "/public"));
handler.use(morgan("dev"));
handler.get("/", (req, res) => {
    res.sendFile(path.join (__dirname + "public/index.html"))
});



server.listen(port, () => {
    require("./server/socketio/")(server);
    console.log("Listen to: ", port);
});



